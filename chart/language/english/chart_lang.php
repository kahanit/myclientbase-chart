<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

$lang['progress_chart'] = 'Progress Chart';
$lang['date_range'] = 'Date Range';
$lang['week'] = 'Week';
$lang['month'] = 'Month';
$lang['year'] = 'Year';
$lang['prev'] = 'Prev';
$lang['next'] = 'Next';

?>