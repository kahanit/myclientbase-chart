<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

$config = array(
	'module_path'			=>	'chart',
	'module_name'			=>	'Chart',
	'module_description'	=>	'Progress chart.',
	'module_author'		=>	'Amit Sidpura',
	'module_homepage'		=>	'http://www.kahanit.com',
	'module_version'		=>	'1.0.0'
);

?>